package auth.csd.kapraran;

import auth.csd.kapraran.network.BayesNetwork;

import java.io.IOException;
import java.util.Scanner;

/**
 *  Created by:
 *  Nikolaos Kapraras    AEM 2166
 *  Vasilis Martidis     AEM 2372
 */
public class ApproxProbInference {
    public static final byte METHOD_REJECTION_SAMPLING = 1;
    public static final byte METHOD_LIKELIHOOD_WEIGHTING = 2;

    Scanner scanner;

    public ApproxProbInference(String bayesNetworkFilename) {
        scanner = new Scanner(System.in);
        BayesNetwork bayesNetwork = null;

        // reads bayes network from file
        try {
            bayesNetwork = BayesNetwork.createFromFile(bayesNetworkFilename);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        // gets user input
        Query query = getQueryInput();
        int samplesNum = getSamplesNumInput();
        byte method = getMethodInput();

        // evaluates the answer
        double[] answer = bayesNetwork.evaluate(query, samplesNum, method);
        query.setAnswer(answer);

        // prints the answer
        query.printAnswer();
    }

    private Query getQueryInput() {
        String qstr;
        while (true) {
            System.out.println("Please enter a query: ");
            qstr = scanner.nextLine().trim();

            // a simple validation check, not perfect
            if (qstr.matches("^(.*)\\((.*)\\|(.*)\\)$"))
                return Query.createFromString(qstr);

            System.err.println("Error! Invalid format, try again.");
        }
    }

    private int getSamplesNumInput() {
        String samplesNumStr;
        while (true) {
            System.out.println("Please enter the number of samples: ");
            samplesNumStr = scanner.nextLine().trim();

            try {
                return Integer.parseInt(samplesNumStr);
            } catch (NumberFormatException e) {
                System.err.println("Error! Please enter an integer.");
            }
        }
    }

    private byte getMethodInput() {
        String methodStr;
        while (true) {
            System.out.println("Please enter the number of the method you want to use: ");
            System.out.println("  1. REJECTION_SAMPLING");
            System.out.println("  2. LIKELIHOOD_WEIGHTING");
            methodStr = scanner.nextLine().trim();

            try {
                Byte method = Byte.parseByte(methodStr);

                if (method == 1 || method == 2)
                    return method;
            } catch (NumberFormatException e) {}

            System.err.println("Error! Please enter 1 or 2.");
        }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Error! Please enter the path to the Bayes Network file");
            System.exit(1);
        }

        new ApproxProbInference(args[0]);
    }

}
