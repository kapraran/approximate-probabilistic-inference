package auth.csd.kapraran;

import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  Created by:
 *  Nikolaos Kapraras    AEM 2166
 *  Vasilis Martidis     AEM 2372
 */
public class Query {
    private String name;
    private String questionVariable;
    private HashMap<String, Boolean> evidences;
    private double[] answer;

    public Query(String name) {
        this.name = name;
        evidences = new HashMap<>();
    }

    public void setQuestionVariable(String questionVariable) {
        this.questionVariable = questionVariable;
    }

    public String getQuestionVariable() {
        return questionVariable;
    }

    public void putEvidence(String key, boolean value) {
        evidences.put(key, value);
    }

    public boolean getEvidenceValue(String key) {
        return evidences.get(key);
    }

    public boolean hasEvidence(String name) {
        return evidences.containsKey(name);
    }

    public Set<String> getEvidenceKeySet() {
        return evidences.keySet();
    }

    public void setAnswer(double[] answer) {
        this.answer = answer;
    }

    public void printAnswer() {
        StringBuilder sb = new StringBuilder();

        sb.append(name);
        sb.append("(" + questionVariable + "|");

        for (String evidence: evidences.keySet()) {
            boolean value = evidences.get(evidence);
            sb.append(evidence + "=" + value + ",");
        }

        // remove last comma
        sb.setLength(sb.length() - 1);
        sb.append(") = ");

        // finally append the probabilities...
        sb.append("<" + String.format(Locale.US, "%.6f", answer[0]) + ", " + String.format(Locale.US, "%.6f", answer[1]) + ">");

        // print answer
        System.out.println(""); // empty line
        System.out.println(sb.toString());
    }

    public static Query createFromString(String qstr) {
        String regexQuery = "(.*)\\((.*)\\|(.*)\\)";
        Pattern patternQuery = Pattern.compile(regexQuery);
        Matcher matcherQuery = patternQuery.matcher(qstr);

        if (matcherQuery.find()) {
            String name = matcherQuery.group(1);
            String variable = matcherQuery.group(2);
            String conditionsStr = matcherQuery.group(3);

            Query query = new Query(name);
            query.setQuestionVariable(variable.trim());

            String regexCondition = "(.*)=(.*)";
            Pattern patternCondition = Pattern.compile(regexCondition);
            for (String conditionStr : conditionsStr.split(",")) {
                Matcher matcherCondition = patternCondition.matcher(conditionStr);

                if (matcherCondition.find()) {
                    String conditionName = matcherCondition.group(1);
                    String conditionValue = matcherCondition.group(2);

                    query.putEvidence(conditionName.trim(), Boolean.parseBoolean(conditionValue));
                }
            }

            return query;
        } else {
            return null;
        }
    }
}
