package auth.csd.kapraran;

import auth.csd.kapraran.network.BayesNetwork;
import auth.csd.kapraran.network.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 *  Created by:
 *  Nikolaos Kapraras    AEM 2166
 *  Vasilis Martidis     AEM 2372
 */
public class Sample {
    public static final byte Q_TRUE = 1;
    public static final byte Q_FALSE = 2;
    public static final byte Q_IGNORE = 3;

    private HashMap<String, Boolean> variables;
    private double weight;
    private static Random generator = new Random(System.currentTimeMillis());

    public Sample() {
        variables = new HashMap<>();
        weight = 1;
    }

    public void setVariable(String name, boolean b) {
        variables.put(name, b);
    }

    public boolean getVariable(String name) {
        return variables.get(name);
    }

    public double getWeight() {
        return weight;
    }

    public void updateWeight(double prob) {
        weight *= prob;
    }

    public static Sample createFromBayesNetwork(BayesNetwork bayesNetwork, Query query, byte method) {
        Sample sample = new Sample();
        ArrayList<Node> nodes = bayesNetwork.getNodes();

        // iterate each node
        for (Node node: nodes) {
            String name = node.getName();
            String[] parents = node.getParents();

            // create the preconditions state to get the probability value
            boolean[] state = new boolean[] {false};
            if (parents.length > 0) {
                state = new boolean[parents.length];
                for (int i=0; i<parents.length; i++)
                    state[i] = sample.getVariable(parents[i]);
            }

            // get the probability to be true based on the state of the parents
            double trueProb = node.getProbability(state);
            boolean value;

            if (method == ApproxProbInference.METHOD_LIKELIHOOD_WEIGHTING) {
                // check if current variable is an evidence
                if (query.hasEvidence(name)) {
                    value = query.getEvidenceValue(name);
                    double prob = value ? trueProb: 1-trueProb;

                    // update sample's weight and set variable value
                    sample.updateWeight(prob);
                    sample.setVariable(name, value);

                    continue;
                }
            }

            // get the random value
            value = generator.nextDouble() <= trueProb;
            sample.setVariable(name, value);
        }

        return sample;
    }

    public byte checkQuery(Query query) {
        // check for every variable to be equal
        for (String key: query.getEvidenceKeySet()) {
            boolean value = query.getEvidenceValue(key);
            if (variables.get(key) != value)
                return Q_IGNORE; // query doesnt match so ignore it
        }

        // if every variable has the same value then return the value of the desired one
        return variables.get(query.getQuestionVariable()) ? Q_TRUE: Q_FALSE;
    }
}
