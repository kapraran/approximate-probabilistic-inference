package auth.csd.kapraran.network;

import auth.csd.kapraran.ApproxProbInference;
import auth.csd.kapraran.Query;
import auth.csd.kapraran.Sample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *  Created by:
 *  Nikolaos Kapraras    AEM 2166
 *  Vasilis Martidis     AEM 2372
 */
public class BayesNetwork {
    ArrayList<Node> nodes;

    public BayesNetwork() {
        nodes = new ArrayList<>();
    }

    public void appendNode(Node node) {
        nodes.add(node);
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public static BayesNetwork createFromFile(String filename) throws IOException {
        BayesNetwork bayesNetwork = new BayesNetwork();
        File file = new File(filename);
        BufferedReader breader = new BufferedReader(new FileReader(file));
        String line;

        for (int i = 0; (line = breader.readLine()) != null; i++) {
            String[] parts = line.split(",");
            int variablesNum = 0;
            int statesNum;

            for (String var : parts) {
                var = var.trim();

                if (!var.matches("^\\d+.\\d+$")) {
                    variablesNum++;
                } else {
                    break;
                }
            }

            statesNum = (int) Math.pow(2, variablesNum - 1);

            if (parts.length != variablesNum + statesNum) {
                System.err.println("Error! Error in line " + i + " of network file. Invalid number of variables+states.");
                System.exit(1);
            }

            Node node = new Node(parts[0].trim(), parts.length - statesNum - 1,  statesNum);
            int stateCounter = statesNum - 1;

            for (int v=1; v<variablesNum; v++) {
                node.setParent(v - 1, parts[v].trim());
            }

            for (int st=variablesNum; st<parts.length; st++) {
                double prob = Double.parseDouble(parts[st]);
                node.setState(stateCounter--, prob);
            }

            bayesNetwork.appendNode(node);
        }

        return bayesNetwork;
    }

    public double[] evaluate(Query query, int samplesNum, byte method) {
        Sample[] samples = new Sample[samplesNum];
        double[] probabilities = new double[2];

        for (int i=0; i<samplesNum; i++)
            samples[i] = Sample.createFromBayesNetwork(this, query, method);

        if (method == ApproxProbInference.METHOD_REJECTION_SAMPLING) {
            long trueNum = 0;
            long falseNum = 0;

            for (int i=0; i<samplesNum; i++) {
                byte res = 0;

                try {
                    res = samples[i].checkQuery(query);
                } catch (NullPointerException e) {
                    System.err.println("Fatal Error!! The variables are case-sensitive, check your query again!");
                    System.exit(1);
                }

                if (res == Sample.Q_TRUE)
                    trueNum++;
                else if (res == Sample.Q_FALSE)
                    falseNum++;
            }

            long sum = trueNum + falseNum;
            probabilities[0] = (trueNum * 1.0)/sum;
            probabilities[1] = (falseNum * 1.0)/sum;

            return probabilities;
        } else {
            double trueWeight = 0;
            double falseWeight = 0;

            for (int i=0; i<samplesNum; i++) {
                Sample sample = samples[i];
                boolean res = sample.getVariable(query.getQuestionVariable());

                if (res)
                    trueWeight += sample.getWeight();
                else
                    falseWeight += sample.getWeight();
            }

            double sum = trueWeight + falseWeight;
            probabilities[0] = trueWeight/sum;
            probabilities[1] = falseWeight/sum;

            return probabilities;
        }

    }
}
