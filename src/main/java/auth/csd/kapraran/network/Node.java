package auth.csd.kapraran.network;

/**
 *  Created by:
 *  Nikolaos Kapraras    AEM 2166
 *  Vasilis Martidis     AEM 2372
 */
public class Node {
    private String name;
    private String[] parents;
    private double[] states; // truth table

    public Node(String name, int parentsNum, int statesNum) {
        this.name = name;
        parents = new String[parentsNum];
        states = new double[statesNum];
    }

    public void setParent(int index, String parent) {
        parents[index] = parent;
    }

    public void setState(int state, double prob) {
        states[state] = prob;
    }

    public String getName() {
        return name;
    }

    public String[] getParents() {
        return parents;
    }

    public double getProbability(boolean[] state) {
        // find the index of the truth table by converting
        // the array of boolean to a decimal
        int index = 0;
        for (int i = 0; i < state.length; i++)
            index += Math.pow(2, state.length - i - 1) * (state[i] ? 1 : 0);

        return states[index];
    }
}
